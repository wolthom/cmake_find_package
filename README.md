Before you can build this project, first build the dependencies by following their instructions

Then, build the main project via:
1. Configuring via:
```
cmake -S . -B build
```

2. Building via:
```
cmake --build build
```
