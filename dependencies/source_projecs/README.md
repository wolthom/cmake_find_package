To build this project as part of the super project, do the following:
1. Configure via:
```
cmake -S . -B build -DCMAKE_INSTALL_PREFIX=..
```

2. Build and install to parent directory via:
```
cmake --build build --target install
```
