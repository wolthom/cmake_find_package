#include <iostream>
#include <calc.h>
#include <printing/printing.h>

int main() {
  int a{10};
  int b{15};
  int c = sum(a, b);

  print_stuff(c);

  return 0;
}
